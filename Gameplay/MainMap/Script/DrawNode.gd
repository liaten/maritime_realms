extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _draw():
	for i in range(200):
		for j in range(200):
			show_point(Color.WHITE,Vector2(35+i,35+j))
#			draw_primitive(PackedVector2Array([Vector2(35+i,35+j)]),PackedColorArray([Color.WHITE]),PackedVector2Array())


func show_point(_color:Color,_position:Vector2):
	var polygon = Polygon2D.new()
	var p = PackedVector2Array()
	var s = 1
	p.append(Vector2(-s,s))
	p.append(Vector2(s,s))
	p.append(Vector2(s,-s))
	p.append(Vector2(-s,-s))
	polygon.polygon = p
	polygon.color = _color
	polygon.position = _position
	add_child(polygon)
