extends Control

@onready var tree:SceneTree = get_tree()
@onready var screen_size:Vector2i = DisplayServer.screen_get_size()
@onready var players:int = int(OS.get_environment("players"))
@onready var land_count_label:Label = $VBoxContainer/LandContainer/LandCountLabel
@onready var starting_area_count_label:Label = $VBoxContainer/StartingAreaContainer/StartingAreaCountLabel
@onready var map :Map= $TextureRect
var land_count:float = 0.5
var starting_area_count:int = 1200


func _on_back_button_pressed():
	tree.change_scene_to_file("res://Menu/ChooseMenu/Scene/ChooseMenu.tscn")


func _on_regenerate_button_pressed():
	map.set_map(land_count,randi())


func _on_land_slider_value_changed(value):
	land_count_label.text = str(value)
	land_count = 1 - value


func _on_starting_area_slider_value_changed(value):
	starting_area_count_label.text = str(value)
	starting_area_count = value


func _on_apply_position_button_pressed():
	if(map.is_place_set && !map.is_position_applied):
		map.expand_start_position(starting_area_count)


func _on_play_button_pressed() -> void:
	pass # Replace with function body.
