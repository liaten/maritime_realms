class_name Map
extends TextureRect

@onready var colors_players:Array = get_colors_players_from_env()
@onready var main_control:=$".."
@onready var player_color:Color = Color.WHITE
var is_position_applied:bool
var is_place_set:bool
var start_pixel:Pixel
var init_image:Image
var current_image:Image
var SEED:int = randi()


func get_colors_players_from_env()-> Array:
	var _colors_players := OS.get_environment("colors_players")
	var json_file := JSON.new()
	json_file.parse(_colors_players)
	return json_file.get_data()


func _ready():
	set_map()


func set_map(land:float = 0.5,_seed:int = SEED):
	current_image = MapGenerator.generate(self.size,land,_seed)
	self.texture = ImageTexture.create_from_image(current_image)
	SEED = _seed


func _on_gui_input(event):
	if event is InputEventMouseButton && event.button_index == MOUSE_BUTTON_LEFT && event.pressed:
		set_starting_point(event)
		return
	if event is InputEventGesture:
		print(event.position)


func place_point(event:InputEventMouseButton):
	start_pixel = Pixel.new(event.position)
#	start_pixel.position = event.position
	var get_results:= MapGenerator.set_start_land_pixel(current_image,start_pixel,player_color)
	current_image = get_results["img"]
	self.texture = ImageTexture.create_from_image(current_image)
	is_place_set = get_results["is_land_pixel_set"]


func set_starting_point(event:InputEventMouseButton):
	if(is_place_set):
		is_position_applied = false
		current_image = MapGenerator.generate(self.size,main_control.land_count,SEED)
	place_point(event)


func expand_start_position(_points:int = 600):
	var points_part :int = _points/16
	var pixels_having_neighbours :Array = [start_pixel]
	for i in range(0,16):
		var expanded_data :Dictionary = MapGenerator.expand_start_pixel(current_image,pixels_having_neighbours,player_color,points_part)
		pixels_having_neighbours = expanded_data["pixels_having_neighbours"]
		current_image = expanded_data["img"]
		self.texture = ImageTexture.create_from_image(current_image)
		await get_tree().create_timer(0.1).timeout
	
	is_position_applied = true
