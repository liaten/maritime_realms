class_name MapGenerator
const SNAPEED_GREEN_COLOR = snapped((Color.DARK_GREEN)[1],0.01)


static func get_placable_pixels_count(img:Image,pixel:Pixel)->int:
	#var destinations_count:int = 8
	var result:int = 0
	for destination in Pixel.DESTINATION:
		var current_relative_destination:Vector2 = Pixel.destination_coords_change[Pixel.DESTINATION[destination]]
		var current_destination:Pixel = Pixel.new(Vector2(
			current_relative_destination.x + pixel.position.x,
			current_relative_destination.y + pixel.position.y,
			))
		if(current_destination.is_placable_land(img)):
			result += 1
	return result


static func find_placable_pixel(img:Image,pixel:Pixel)->Pixel:
	#var destinations_count:int = 8
	var placable_lands = []
	for destination in Pixel.DESTINATION:
		var current_relative_destination:Vector2 = Pixel.destination_coords_change[Pixel.DESTINATION[destination]]
		var current_destination:Pixel = Pixel.new(Vector2(
			current_relative_destination.x + pixel.position.x,
			current_relative_destination.y + pixel.position.y,
			))
		if(current_destination.is_placable_land(img)):
			placable_lands.append(current_destination)
			#return(current_destination)
	if(placable_lands.size() == 0):
		return Pixel.new(Vector2(-1,-1))
	if(placable_lands.size() == 1):
		return placable_lands[0]
	return placable_lands.pick_random()


static func expand_start_pixel(img:Image, _pixels_having_neighbours:Array, color:Color, _points_left:int):
	var points_left:int = _points_left
	var pixels_having_neighbours:Array = _pixels_having_neighbours
	while(points_left>0):
		if(pixels_having_neighbours.size() == 0):
			break
		var rand_number:int = randf() * 8
		randomize()
		var pixels_having_neighbours_random_element_id:int = 0
		if(int(pixels_having_neighbours.size() * 0.3) == 0):
			pixels_having_neighbours_random_element_id = rand_number % pixels_having_neighbours.size()
		else:
			var m :float = 0.0
			var deleting_number :int = 0
			while(deleting_number == 0):
				m += 0.2
				deleting_number = int(pixels_having_neighbours.size() * m)
			if(randf()>0.5):
				pixels_having_neighbours_random_element_id = pixels_having_neighbours.size() - (rand_number % deleting_number) - 1
			else:
				pixels_having_neighbours_random_element_id = rand_number % deleting_number
			
		var placable_pixel = find_placable_pixel(img,pixels_having_neighbours[pixels_having_neighbours_random_element_id])
		if(placable_pixel.position != Vector2(-1,-1)):
			update_pixel(img,placable_pixel.position.x,placable_pixel.position.y,color)
			pixels_having_neighbours.append(placable_pixel)
		points_left-=1
		if(get_placable_pixels_count(img,pixels_having_neighbours[pixels_having_neighbours_random_element_id]) == 0):
			pixels_having_neighbours.remove_at(pixels_having_neighbours_random_element_id)
	
	return {
		"img":img,
		"pixels_having_neighbours":pixels_having_neighbours,
		}


static func set_start_land_pixel(img:Image,_start_pixel:Pixel,color:Color) ->Dictionary:
	var is_land_pixel_set:bool = false
	if _start_pixel.is_placable_land(img):
		update_pixel(img,_start_pixel.position.x,_start_pixel.position.y,color)
		is_land_pixel_set = true
	return {"img":img,"is_land_pixel_set":is_land_pixel_set}


static func update_pixel(img:Image,x:int,y:int,color:Color):
	img.set_pixel(x, y, color)


static func generate(imgSize : Vector2,_land_level:float,_seed:int)-> Image:
	var genE: FastNoiseLite = FastNoiseLite.new()
	genE.seed = _seed
	
	var img := Image.create((int)(imgSize.x), (int)(imgSize.y), false, Image.FORMAT_RGBH)
	var img_size_x := img.get_size().x
	var img_size_y := img.get_size().y
	
	var noises :Array = []
	noises.resize(img_size_x)
	var noises_max:float
	var noises_min:float
	var is_max_min_set:bool = false
	
	for x in range(img_size_x):
		noises[x] = []
		noises[x].resize(img_size_y)
		for y in range(img_size_y):
			noises[x][y] = genE.get_noise_2d(x,y)
			if(!is_max_min_set):
				noises_max = noises[x][y]
				noises_min = noises[x][y]
				is_max_min_set = true
			else:
				if(noises[x][y] > noises_max):
					noises_max = noises[x][y]
				if(noises[x][y] < noises_min):
					noises_min = noises[x][y]
	noises = normalize(noises,0,1,noises_max,noises_min)
	
	for x in range(img_size_x):
		for y in range(img_size_y):
			var noise:float = noises[x][y]
			var color:Color
			if(noise<_land_level):
				color = Color.DARK_BLUE
			else:
				color = Color.DARK_GREEN
			img.set_pixel(x, y, color)
	return img

static func normalize(array:Array,t_min,t_max,a_max,a_min):
	var norm_arr := []
	norm_arr.resize(array.size())
	var diff = t_max - t_min
	var diff_arr = a_max - a_min
	for x in range(array.size()):
		norm_arr[x] = []
		norm_arr[x].resize(array[x].size())
		for y in range (array[x].size()):
			norm_arr[x][y] = (((array[x][y] - a_min)*diff)/diff_arr) + t_min
	return norm_arr
