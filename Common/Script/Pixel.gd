class_name Pixel
var position:Vector2
var all_neighbours_set:bool = false
var neighbours:Array = [] # of Pixels


enum DESTINATION{
	TOP_LEFT,
	TOP,
	TOP_RIGHT,
	LEFT,
	RIGHT,
	BOTTOM_LEFT,
	BOTTOM,
	BOTTOM_RIGHT
}


static var destination_coords_change:Array = [
	Vector2(-1,-1),		# TOP_LEFT
	Vector2(0,-1),		# TOP
	Vector2(0,1),		# TOP_RIGHT
	Vector2(-1,0),		# LEFT
	Vector2(1,0),		# RIGHT
	Vector2(1,-1),		# BOTTOM_LEFT
	Vector2(1,0),		# BOTTOM
	Vector2(1,1),		# BOTTOM_RIGHT
]


func _init(_position:Vector2):
	position = _position


func is_land(img:Image)->bool:
	var pixel_color := img.get_pixel(position.x,position.y)
	return snapped(pixel_color[1],0.01) == MapGenerator.SNAPEED_GREEN_COLOR

func is_not_out_of_bounds(img:Image)->bool:
	if(
		self.position.x > img.data.width ||
		self.position.x < 0 ||
		self.position.y > img.data.height ||
		self.position.y < 0
		):
			return false
	#print(img.data.width)
	return true

func is_placable_land(img:Image)->bool:
	#if(!is_not_out_of_bounds(img)):
		#print('not')
	return is_not_out_of_bounds(img) && is_land(img)
