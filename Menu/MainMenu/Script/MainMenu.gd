extends Node

@onready var tree:SceneTree = get_tree()

# Called when the node enters the scene tree for the first time.
func _ready():
	Engine.max_fps = 15


func _on_exit_game_button_pressed():
	tree.quit()


func _on_start_game_button_pressed():
	tree.change_scene_to_file("res://Menu/ChooseMenu/Scene/ChooseMenu.tscn")
