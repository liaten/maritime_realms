extends VBoxContainer

@onready var tree:SceneTree = get_tree()
@onready var players_count_label:Label = $PlayersCountContainer/PlayersCountLabel
var colors = [
	"FF0000",
	"A52A2A",
	"FFC0CB",
	"FFFF00",
	"FFA500",
	"800080",
	"008080",
	"800000",
]


func _ready():
	pass


func _on_back_button_pressed():
	tree.change_scene_to_file("res://Menu/MainMenu/Scene/MainMenu.tscn")


func _on_next_button_pressed():
	set_players()
	set_colors_players()
	tree.change_scene_to_file("res://Gameplay/MainMap/Scene/Map.tscn")


func _on_players_slider_value_changed(value):
	players_count_label.text = str(value)


func set_players():
	OS.set_environment("players",players_count_label.text)


func set_colors_players():
	var players:int = int(players_count_label.text)
	var colors_players :Array= []
	for i in range(players):
		var color_position:int = randi()%colors.size()
		var color:String = colors[color_position]
		colors_players.push_back(color)
		colors.remove_at(color_position)
	OS.set_environment("colors_players",JSON.stringify(colors_players))
