# Godot 4.2 Android strategy game

# How to run

## 1. Download latest Godot IDE (now 4.2, will be updated)

[LINK](https://godotengine.org/download/)

![download](img/godot-download-preview.png)

## 2. Import project.godot file from IDE
